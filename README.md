# common-pipelines

## Usage
To use the pipelines in your project you need to include this project in your GitLab CI configuration.

### Example (Python Pipeline)
```
include:
    - project: 'wahl-sec/common-piplines'
      ref: master
      file: '/python/python.gitlab-ci.yml'
```

## Configuration
Some of the pipeline configurations can use some sort of configuration using environment variables, so in order to configure you need to set the variables in your project on project/pipeline/global level.

## Requests
Feel free to request any tool you wish to include in any general pipeline, or create a merge request with your configuration file.
